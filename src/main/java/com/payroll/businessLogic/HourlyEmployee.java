package com.payroll.businessLogic;

import java.util.ArrayList;
import java.util.List;

public class HourlyEmployee extends Employee {

    private static final double OVERTIME_BONUS_RATIO = 1.5;

    private static final double MINIMUM_WAGE = 115;

    private double hourlyRate;
    private List<TimeCard> timeCards;

    public HourlyEmployee(String name, double hourlyRate) {
        super(name);

        if (hourlyRate < MINIMUM_WAGE) {
            throw new IllegalArgumentException(hourlyRate + "is lower than the regulated minimum wage " + MINIMUM_WAGE);
        }

        this.timeCards = new ArrayList<TimeCard>();
        this.hourlyRate = hourlyRate;
    }


    public List<TimeCard> getTimeCards() {
        return this.timeCards;
    }


    public void addTimeCard(TimeCard timeCard) {
        timeCards.add(timeCard);
    }

    @Override
    public double calculatePay() {
        List<TimeCard> timeCards = this.getTimeCards();

        double salary = 0.0;

        for (TimeCard timeCard : timeCards) {
            timeCard.process();
            double todaySalary = (timeCard.getBaseHours() + timeCard.getOTHours() * OVERTIME_BONUS_RATIO) * hourlyRate;
            salary += todaySalary;
        }

        return salary;
    }

    @Override
    public boolean checkLimitExceed() {
        throw new UnsupportedOperationException();
    }
}
